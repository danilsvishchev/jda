package enums;

public enum TaskStatus {
    BACKLOG,
    TO_ANALYSE,
    TO_DO,
    IN_PROGRESS,
    IN_REVIEW,
    TO_TEST,
    TO_TEST_CASE,
    TO_CASE_ANALYSE,
    IN_TESTING,
    TO_STAGE,
    DONE
}
