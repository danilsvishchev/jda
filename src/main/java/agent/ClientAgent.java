package agent;

import enums.TaskStatus;
import forms.ClientForm;
import jade.core.AID;
import jade.core.NotFoundException;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import model.InternalMessage;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import java.awt.EventQueue;

/**
 * Агента A14 <br/>
 * Ожидает выполненные задания от {@link DevOpsAgent} <br/>
 * Отдает задачи и фитбек агенту-техническому руководителю {@link  ClientAgent}
 */
@Getter
@Slf4j
public class ClientAgent extends AbstractAgent {
    private ClientForm form;
    private AID techDirector;

    public static final String AGENT_PUBLISHED_NAME = "Client";

    @Override
    protected String getPublishingType() {
        return "use-of-functionality";
    }

    @Override
    protected String getPublishingName() {
        return AGENT_PUBLISHED_NAME;
    }

    @Override
    protected void setup() {
        super.setup();
        this.form = new ClientForm(this);
        EventQueue.invokeLater(() -> form.setVisible(true));
        addBehaviour(new MessageService());
    }

    /**
     * Поиск агента-технического директора {@link TechnicalDirectorAgent}
     */
    private void findTechDirector() {
        try {
            techDirector = findPublishedAgents("task-processing", "Tech director").stream()
                                                                                                     .findFirst()
                                                                                                     .orElseThrow(() -> new NotFoundException("Tech director was not found in yellow pages service"));
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Отправка сообщения техническому директору {@link TechnicalDirectorAgent}
     *
     * @param message сообщение для отправки
     */
    public void sendMessage(String message) {
        log.info("Try to process message {}", getName());
        addBehaviour(new OneShotBehaviour() {
            @Override
            public void action() {
                findTechDirector();

                if (techDirector != null) {
                    ACLMessage cfp = new ACLMessage(ACLMessage.INFORM);
                    log.info("Agent {} receive message: {}", getName(), message);

                    cfp.setConversationId(TaskStatus.BACKLOG.toString());
                    cfp.addReceiver(techDirector);
                    cfp.setContent(message);
                    log.info("Try to send received message from agent {} to {}", getName(), techDirector.getName());

                    myAgent.send(cfp);
                    log.info("Agent {} successfully send message: [messageType: {}, messageContent: {}] to [name: {}]", getName(), ACLMessage.getPerformative(cfp.getPerformative()), cfp.getContent(), techDirector.getName());
                }
            }
        });
    }

    /**
     * Обработчик входящих сообщений <br/>
     *
     * Осуществляется получение конечного продукта и отправка задач агенту {@link TechnicalDirectorAgent}
     */
    class MessageService extends CyclicBehaviour {

        @Override
        public void action() {
            log.info("Try to receive message by {}", getName());
            MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                           MessageTemplate.MatchConversationId(TaskStatus.DONE.toString()));
            ACLMessage message = myAgent.receive(template);

            if (message != null) {
                String receiveContent = message.getContent();
                InternalMessage internalMessage = readAndProcessMessage(receiveContent);

                log.info("Agent {} receive message: {}", getName(), receiveContent);
                String content = writeMessageAsString(internalMessage, receiveContent);

                DefaultListModel<String> model = (DefaultListModel<String>) form.getMessageList().getModel();
                model.addElement(internalMessage.getContent());
                JOptionPane.showMessageDialog(null, "Receive new message \"" + internalMessage.getContent() + "\" from " + message.getSender().getName(), "Information", JOptionPane.INFORMATION_MESSAGE);

                log.info("Message processing successfully completed {}", content);
            } else {
                log.info("Agent {} could not receive message. Wait new messages", getName());
                block();
            }
        }
    }

}
