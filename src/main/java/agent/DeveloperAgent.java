package agent;

import enums.TaskStatus;
import jade.core.AID;
import jade.core.NotFoundException;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import model.InternalMessage;

/**
 * Агент A3-A5 <br/>
 * Ожидает сообщений-заданий на обработку от {@link ProjectManagerAgent} <br/>
 * Отдает на выполнение агенту-руководителю команды {@link TeamLeadAgent}
 */
@Getter
@Slf4j
public class DeveloperAgent extends AbstractAgent {
    private AID teamLead;

    @Override
    protected String getPublishingType() {
        return "functional development";
    }

    @Override
    public String getPublishingName() {
        return "Developer " + System.currentTimeMillis();
    }

    @Override
    protected void setup() {
        super.setup();

        findTeamLead();

        addBehaviour(new MessageService());
    }

    /**
     * Поиск агента руководителя команды разработки {@link TeamLeadAgent} для проверки и сбора выполненных тикетов
     */
    private void findTeamLead() {
        try {
            teamLead = findPublishedAgents("task-accepting-and-merging", "Team lead").stream()
                                                                                                        .findFirst()
                                                                                                        .orElseThrow(() -> new NotFoundException("Team lead" + " was not found in yellow pages service"));
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Обработчик входящих сообщений <br/>
     *
     * Осуществляется получение сообщение по заданному шаблону и отправка агенту {@link TeamLeadAgent}
     */
    class MessageService extends CyclicBehaviour {

        @Override
        public void action() {
            if (teamLead == null) {
                log.warn("Receiver AID not found. Any messages could not be processed");
                findTeamLead();
                return;
            }

            log.info("Try to receive message by {}", getName());
            MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                           MessageTemplate.MatchConversationId(TaskStatus.IN_PROGRESS.toString()));
            ACLMessage message = myAgent.receive(template);

            if (message != null) {
                ACLMessage cfp = new ACLMessage(ACLMessage.INFORM);
                String receiveContent = message.getContent();
                InternalMessage internalMessage = readAndProcessMessage(receiveContent);

                log.info("Agent {} receive message: {}", getName(), receiveContent);
                String content = writeMessageAsString(internalMessage, receiveContent);

                cfp.setConversationId(TaskStatus.IN_REVIEW.toString());
                cfp.addReceiver(teamLead);
                cfp.setContent(content);
                log.info("Try to send received message from agent {} to {}", getName(), teamLead.getName());

                myAgent.send(cfp);
                log.info("Agent {} successfully send message: [messageType: {}, messageContent: {}] to [name: {}]", getName(), ACLMessage.getPerformative(cfp.getPerformative()), content, teamLead.getName());
            } else {
                log.info("Agent {} could not receive message. Wait new messages", getName());
                block();
            }

        }
    }

}
