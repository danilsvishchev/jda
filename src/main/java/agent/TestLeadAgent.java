package agent;

import com.fasterxml.jackson.core.JsonProcessingException;
import enums.TaskStatus;
import forms.TestLeadForm;
import jade.core.AID;
import jade.core.Agent;
import jade.core.NotFoundException;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import model.InternalMessage;

import javax.annotation.Nonnull;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import java.awt.EventQueue;
import java.util.Optional;
import java.util.Set;

/**
 * Агента A9 (SWITCH) <br/>
 * Ожидает сообщений-заданий на обработку от {@link TestManagerAgent} <br/>
 * Отдает на выполнение агенту-тестировщику {@link TesterAgent}
 */
@Getter
@Slf4j
public class TestLeadAgent extends AbstractAgent {
    private Set<AID> testers;
    private TestLeadForm form;

    public static final String AGENT_PUBLISHED_NAME = "Test Lead";

    @Override
    protected String getPublishingType() {
        return "task-test-processing";
    }

    @Override
    protected String getPublishingName() {
        return AGENT_PUBLISHED_NAME;
    }

    @Override
    protected void setup() {
        super.setup();
        this.form = new TestLeadForm(this);
        EventQueue.invokeLater(() -> form.setVisible(true));

        findTesters();

        addBehaviour(new MessageService());
        addBehaviour(new TesterChecker(this, 60_000L));
    }

    /**
     * Поиск агентов тестировщиков {@link TesterAgent} для тестирования поставленных сообщений-тикетов
     */
    public void findTesters() {
        try {
            testers = Optional.ofNullable(findPublishedAgents("functional testing", null))
                              .orElseThrow(() -> new NotFoundException("Testers was not found in yellow pages service"));

            DefaultListModel<AID> model = (DefaultListModel<AID>) form.getAgentList().getModel();
            model.removeAllElements();
            testers.forEach(model::addElement);
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Отправка сообщения агенту-тестировщику {@link TesterAgent}
     *
     * @param message сообщение для отправки
     * @param agent получатель сообщения
     */
    public void sendMessage(String message, @Nonnull AID agent) {
        log.info("Try to process message {}", getName());
        addBehaviour(new OneShotBehaviour() {
            @Override
            public void action() {
                ACLMessage cfp = new ACLMessage(ACLMessage.INFORM);
                InternalMessage internalMessage = TestLeadAgent.super.readAndProcessMessage(message);

                log.info("Agent {} receive message: {}", getName(), message);
                String content = writeMessageAsString(internalMessage, message);

                cfp.setConversationId(TaskStatus.IN_TESTING.toString());
                cfp.addReceiver(agent);
                cfp.setContent(content);
                log.info("Try to send received message from agent {} to {}", getName(), agent.getName());

                myAgent.send(cfp);
                log.info("Agent {} successfully send message: [messageType: {}, messageContent: {}] to testers: {}", getName(), ACLMessage.getPerformative(cfp.getPerformative()), content, agent.getName());
            }
        });
    }

    /**
     * Обработчик входящих сообщений <br/>
     *
     * Осуществляется получение сообщение по заданному шаблону и отправка агенту {@link TestLeadAgent}
     */
    class MessageService extends CyclicBehaviour {

        @Override
        public void action() {
            log.info("Try to receive message by {}", getName());
            MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                           MessageTemplate.MatchConversationId(TaskStatus.TO_TEST_CASE.toString()));
            ACLMessage message = myAgent.receive(template);

            if (message != null) {
                InternalMessage internalMessage = readAndProcessMessage(message.getContent());
                log.info("Agent {} receive message: {}", getName(), message.getContent());

                DefaultListModel<String> model = (DefaultListModel<String>) form.getMessageList().getModel();
                model.addElement(message.getContent());
                JOptionPane.showMessageDialog(null, "Receive new message \"" + internalMessage.getContent() + "\" from " + message.getSender().getName(), "Information", JOptionPane.INFORMATION_MESSAGE);

                log.info("Message successfully add to UI");
            } else {
                log.info("Agent {} could not receive message. Wait new messages", getName());
                block();
            }
        }
    }

    /**
     * Поведение, отслеживающее наличие активных агентов-тестировщиков
     */
    class TesterChecker extends TickerBehaviour {
        public TesterChecker(Agent agent, long period) {
            super(agent, period);
        }

        @Override
        protected void onTick() {
            findTesters();
        }
    }

    /**
     * Переопределение метода абстрактного класса {@link AbstractAgent#readAndProcessMessage}. <br/>
     * Десериазация полученного сообщения
     *
     * @param receiveMessage полученное сообщение
     * @return объект внутреннего сообщения
     */
    @Override
    protected InternalMessage readAndProcessMessage(String receiveMessage) {
        try {
            return mapper.readValue(receiveMessage, InternalMessage.class);
        } catch (JsonProcessingException e) {
            log.error("Couldn't deserialize message content: {}", receiveMessage);
            throw new IllegalArgumentException(e.getMessage());
        }
    }
}
