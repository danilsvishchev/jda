package agent;

import enums.TaskStatus;
import forms.TechDirectorForm;
import jade.core.AID;
import jade.core.NotFoundException;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import model.InternalMessage;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import java.awt.EventQueue;
import java.util.Collections;
import java.util.UUID;

/**
 * Агент A0 <br/>
 * Запускает процесс обработки сообщений <br/>
 * Отдает на выполнение агенту-менеджеру (продуктовому) {@link TestDirectorAgent}
 */
@Getter
@Slf4j
public class TechnicalDirectorAgent extends AbstractAgent {
    private TechDirectorForm form;
    private AID deliveryManager;

    public static final String AGENT_PUBLISHED_NAME = "Tech director";

    @Override
    protected String getPublishingType() {
        return "task-processing";
    }

    @Override
    public String getPublishingName() {
        return AGENT_PUBLISHED_NAME;
    }

    @Override
    protected void setup() {
        super.setup();
        this.form = new TechDirectorForm(this);
        EventQueue.invokeLater(() -> form.setVisible(true));
        addBehaviour(new MessageService());
    }

    /**
     * Отправка сообщения delivery-manager команды разработки
     *
     * @param message сообщение для отправки
     */
    public void sendMessage(String message) {
        log.info("Try to process message {}", getName());
        addBehaviour(new OneShotBehaviour() {
            @Override
            public void action() {
                findDeliveryManager();

                if (deliveryManager != null) {
                    ACLMessage cfp = new ACLMessage(ACLMessage.INFORM);
                    InternalMessage internalMessage = InternalMessage.builder()
                                                                     .id(UUID.randomUUID().toString())
                                                                     .authorAID(getAID())
                                                                     .createTimestamp(System.currentTimeMillis())
                                                                     .handlers(Collections.singletonList(getAID()))
                                                                     .content(message)
                                                                     .build();

                    String content = writeMessageAsString(internalMessage, message);

                    cfp.setConversationId(TaskStatus.TO_ANALYSE.toString());
                    cfp.addReceiver(deliveryManager);
                    cfp.setContent(content);

                    myAgent.send(cfp);
                    log.info("Successfully send message: [messageType: {}, messageContent: {}] to [name: {}]", ACLMessage.getPerformative(cfp.getPerformative()), content, deliveryManager.getName());
                }
            }
        });
    }

    /**
     * Поиск агента менеждера продукта {@link DeliveryManagerAgent} в сервисе желтых страниц
     */
    private void findDeliveryManager() {
        try {
            // TODO:подумать, может не стоит собирать стек-трейс (это довольно-таки ресурсо-затратная операция)
            deliveryManager = findPublishedAgents("task-processing",
                                                  DeliveryManagerAgent.AGENT_PUBLISHED_NAME).stream()
                                                                                            .findFirst()
                                                                                            .orElseThrow(() -> new NotFoundException(DeliveryManagerAgent.AGENT_PUBLISHED_NAME + " was not found in yellow pages service"));
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        }
    }


    /**
     * Обработчик входящих сообщений <br/>
     *
     * Осуществляется получение сообщение по заданному шаблону {@link ClientAgent} и отправку агенту {@link DeliveryManagerAgent}
     */
    class MessageService extends CyclicBehaviour {

        @Override
        public void action() {
            log.info("Try to receive message by {}", getName());
            MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                           MessageTemplate.MatchConversationId(TaskStatus.BACKLOG.toString()));
            ACLMessage message = myAgent.receive(template);

            if (message != null) {
                log.info("Agent {} receive message: {}", getName(), message.getContent());

                DefaultListModel<String> model = (DefaultListModel<String>) form.getMessageList().getModel();
                model.addElement(message.getContent());
                JOptionPane.showMessageDialog(null, "Receive new message \"" + message.getContent() + "\" from " + message.getSender().getName(), "Information", JOptionPane.INFORMATION_MESSAGE);

                log.info("Message successfully add to UI");
            } else {
                log.info("Agent {} could not receive message. Wait new messages", getName());
                block();
            }

        }
    }

}
