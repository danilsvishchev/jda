package agent;

import enums.TaskStatus;
import jade.core.AID;
import jade.core.NotFoundException;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import model.InternalMessage;

/**
 * Агента A7 <br/>
 * Ожидает сообщений-заданий на обработку от {@link TeamLeadAgent} <br/>
 * Отдает на выполнение агенту-менеджеру {@link TestManagerAgent}
 */
@Getter
@Slf4j
public class TestDirectorAgent extends AbstractAgent {
    private AID testManager;

    public static final String AGENT_PUBLISHED_NAME = "Test Director";

    @Override
    protected String getPublishingType() {
        return "task-test-processing";
    }

    @Override
    protected String getPublishingName() {
        return AGENT_PUBLISHED_NAME;
    }

    @Override
    protected void setup() {
        super.setup();

        findTestingManager();

        addBehaviour(new MessageService());
    }

    /**
     * Поиск агента менеджера тестирования {@link TestManagerAgent} в сервисе желтых страниц
     */
    private void findTestingManager() {
        try {
            testManager = findPublishedAgents("task-test-processing", "Test Manager").stream()
                                                                                                        .findFirst()
                                                                                                        .orElseThrow(() -> new NotFoundException("Testing lead was not found in yellow pages service"));
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Обработчик входящих сообщений <br/>
     *
     * Осуществляется получение сообщение по заданному шаблону и отправка агенту {@link TestManagerAgent}
     */
    class MessageService extends CyclicBehaviour {

        @Override
        public void action() {
            if (testManager == null) {
                log.warn("Receiver AID not found. Any messages could not be processed");
                findTestingManager();
                return;
            }

            log.info("Try to receive message by {}", getName());
            MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                           MessageTemplate.MatchConversationId(TaskStatus.TO_CASE_ANALYSE.toString()));
            ACLMessage message = myAgent.receive(template);

            if (message != null) {
                ACLMessage cfp = new ACLMessage(ACLMessage.INFORM);
                String receiveContent = message.getContent();
                InternalMessage internalMessage = readAndProcessMessage(receiveContent);

                log.info("Agent {} receive message: {}", getName(), receiveContent);
                String content = writeMessageAsString(internalMessage, receiveContent);

                cfp.setConversationId(TaskStatus.TO_TEST.toString());
                cfp.addReceiver(testManager);
                cfp.setContent(content);
                log.info("Try to send received message from agent {} to {}", getName(), testManager.getName());

                myAgent.send(cfp);
                log.info("Agent {} successfully send message: [messageType: {}, messageContent: {}] to [name: {}]", getName(), ACLMessage.getPerformative(cfp.getPerformative()), content, testManager.getName());
            } else {
                log.info("Agent {} could not receive message. Wait new messages", getName());
                block();
            }
        }
    }

}
