package agent;

import enums.TaskStatus;
import jade.core.AID;
import jade.core.NotFoundException;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import model.InternalMessage;

/**
 * Агента A8 <br/>
 * Ожидает сообщений-заданий на обработку от {@link TestDirectorAgent} <br/>
 * Отдает на выполнение агенту-менеджеру {@link TestLeadAgent}
 */
@Getter
@Slf4j
public class TestManagerAgent extends AbstractAgent {
    private AID testLead;

    public static final String AGENT_PUBLISHED_NAME = "Test Manager";

    @Override
    protected String getPublishingType() {
        return "task-test-processing";
    }

    @Override
    protected String getPublishingName() {
        return AGENT_PUBLISHED_NAME;
    }


    @Override
    protected void setup() {
        super.setup();

        findTestingLead();

        addBehaviour(new MessageService());
    }

    /**
     * Поиск агента руководителя команды тестирования {@link TestLeadAgent} в сервисе желтых страниц
     */
    private void findTestingLead() {
        try {
            testLead = findPublishedAgents("task-test-processing", "Test Lead").stream()
                    .findFirst()
                    .orElseThrow(() -> new NotFoundException("Testing lead was not found in yellow pages service"));
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Обработчик входящих сообщений <br/>
     *
     * Осуществляется получение сообщение по заданному шаблону и отправка агенту {@link TestLeadAgent}
     */
    class MessageService extends CyclicBehaviour {

        @Override
        public void action() {
            if (testLead == null) {
                log.warn("Receiver AID not found. Any messages could not be processed");
                findTestingLead();
                return;
            }

            log.info("Try to receive message by {}", getName());
            MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                           MessageTemplate.MatchConversationId(TaskStatus.TO_TEST.toString()));
            ACLMessage message = myAgent.receive(template);

            if (message != null) {
                ACLMessage cfp = new ACLMessage(ACLMessage.INFORM);
                String receiveContent = message.getContent();
                InternalMessage internalMessage = readAndProcessMessage(receiveContent);

                log.info("Agent {} receive message: {}", getName(), receiveContent);
                String content = writeMessageAsString(internalMessage, receiveContent);

                cfp.setConversationId(TaskStatus.TO_TEST_CASE.toString());
                cfp.addReceiver(testLead);
                cfp.setContent(content);
                log.info("Try to send received message from agent {} to {}", getName(), testLead.getName());

                myAgent.send(cfp);
                log.info("Agent {} successfully send message: [messageType: {}, messageContent: {}] to [name: {}]", getName(), ACLMessage.getPerformative(cfp.getPerformative()), content, testLead.getName());
            } else {
                log.info("Agent {} could not receive message. Wait new messages", getName());
                block();
            }
        }
    }

}
