package agent;

import jade.core.Agent;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestAgent extends Agent {
    @Override
    protected void setup() {
        log.info("Application start working");
    }

    @Override
    protected void takeDown() {
        log.info("Agent {} is terminating", getLocalName());
    }
}
