package agent;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import lombok.extern.slf4j.Slf4j;
import model.InternalMessage;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Абстрактный класс для инициализации объекта-агента в системе распределенного алгоритма
 */
@Slf4j
public abstract class AbstractAgent extends Agent {
    /**
     * Маппер для сериализации/десериализации объектов в JSON
     */
    protected ObjectMapper mapper;

    /**
     * Установка типа агента для публикации в сервисе желтых страниц
     *
     * @return тип агента при публикации в сервисе желтых страниц
     */
    protected abstract String getPublishingType();

    /**
     * Установка имени агента для публикации в сервисе желтых страниц
     *
     * @return имя агента при публикации в сервисе желтых страниц
     */
    protected abstract String getPublishingName();

    /**
     * Инициализация агента в системе
     */
    @Override
    protected void setup() {
        log.info("Agent {} start working", getName());

        this.mapper = getObjectMapper();

        servicePublication(getPublishingType());
    }

    /**
     * Деинициализация агента в системе
     */
    @Override
    protected void takeDown() {
        try {
            DFService.deregister(this);
        } catch (FIPAException e) {
            log.error("An error occurred while deactivation agent {} service", getName());
        }
        log.warn("Agent {} stop working", getName());
    }

    /**
     * Публикация агента в сервисе желтых страниц
     */
    protected void servicePublication(@Nonnull String type) {
        log.info("Try to publish agent {} in yellow pages service", getName());

        DFAgentDescription dfAgentDescription = new DFAgentDescription();
        ServiceDescription serviceDescription = new ServiceDescription();

        dfAgentDescription.setName(getAID());
        serviceDescription.setType(type);
        serviceDescription.setName(getPublishingName());
        dfAgentDescription.addServices(serviceDescription);

        try {
            DFService.register(this, dfAgentDescription);
            log.info("Agent {} successfully published in yellow pages", getName());
        } catch (FIPAException e) {
            log.error("Agent {} publication in yellow pages unavailable", getName());
            takeDown();
        }
    }

    /**
     * Поиск агентов в сервисе желтых страниц
     *
     * @param agentType тип агента
     * @param agentName наименование агента
     * @return множество идентификаторов найденных объектов
     */
    protected Set<AID> findPublishedAgents(@Nonnull String agentType, @Nullable String agentName) {
        log.info("Try to find agents in yellow pages service by name: {}", agentName);

        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription serviceDescription = new ServiceDescription();

        if (agentName != null) {
            serviceDescription.setName(agentName);
        }
        serviceDescription.setType(agentType);
        template.addServices(serviceDescription);

        try {
            return Arrays.stream(DFService.search(this, template))
                    .map(DFAgentDescription::getName)
                    .collect(Collectors.toSet());
        } catch (FIPAException ex) {
            log.error("An error occurred while searching for a {}: {}", agentName, ex.getMessage());
        }

        return Collections.emptySet();
    }

    /**
     * Чтение и первичная обработка полученного сообщения
     *
     * @param receiveMessage полученное сообщение
     * @return обработанное внутреннее сообщение
     */
    protected InternalMessage readAndProcessMessage(String receiveMessage) {
        try {
            InternalMessage internalMessage = mapper.readValue(receiveMessage, InternalMessage.class);

            internalMessage.setUpdateTimestamp(System.currentTimeMillis());
            internalMessage.getHandlers().add(getAID());

            return internalMessage;
        } catch (JsonProcessingException e) {
            log.error("Couldn't deserialize message content: {}", receiveMessage);
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    /**
     * Десериализация сообщения
     *
     * @param message объект внутреннего сообщения
     * @param defaultMessage сообщение по умолчанию
     * @return сообщение в формате JSON
     */
    protected String writeMessageAsString(InternalMessage message, String defaultMessage) {
        try {
            return mapper.writeValueAsString(message);
        } catch (JsonProcessingException e) {
            log.warn("Couldn't serialize internal message, send default text");
            return defaultMessage;
        }
    }

    /**
     * Конфигурирование маппера
     *
     * @return сконфигурированный маппер
     */
    protected ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
