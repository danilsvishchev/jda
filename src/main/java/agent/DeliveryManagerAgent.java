package agent;

import enums.TaskStatus;
import jade.core.AID;
import jade.core.NotFoundException;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import model.InternalMessage;

/**
 * Агент A1 <br/>
 * Ожидает сообщений-заданий на обработку от {@link TechnicalDirectorAgent} <br/>
 * Отдает на выполнение агенту-менеджеру (проектному) {@link ProjectManagerAgent}
 */
@Getter
@Slf4j
public class DeliveryManagerAgent extends AbstractAgent {
    private AID projectManager;

    public static final String AGENT_PUBLISHED_NAME = "Delivery manager";

    @Override
    protected String getPublishingType() {
        return "task-processing";
    }

    @Override
    protected String getPublishingName() {
        return AGENT_PUBLISHED_NAME;
    }

    @Override
    protected void setup() {
        super.setup();

        findPM();

        addBehaviour(new MessageService());
    }

    /**
     * Поиск агента менеджера проекта {@link ProjectManagerAgent} в сервисе желтых страниц
     */
    private void findPM() {
        try {
            projectManager = findPublishedAgents("task-processing", ProjectManagerAgent.AGENT_PUBLISHED_NAME).stream()
                    .findFirst()
                    .orElseThrow(() -> new NotFoundException(ProjectManagerAgent.AGENT_PUBLISHED_NAME + " was not found in yellow pages service"));
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Обработчик входящих сообщений <br/>
     *
     * Осуществляется получение сообщение по заданному шаблону и отправка агенту {@link ProjectManagerAgent}
     */
    class MessageService extends CyclicBehaviour {

        @Override
        public void action() {
            if (projectManager == null) {
                log.warn("Receiver AID not found. Any messages could not be processed");
                findPM();
                return;
            }

            log.info("Try to receive message by {}", getName());
            MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                           MessageTemplate.MatchConversationId(TaskStatus.TO_ANALYSE.toString()));
            ACLMessage message = myAgent.receive(template);

            if (message != null) {
                ACLMessage cfp = new ACLMessage(ACLMessage.INFORM);
                String receiveContent = message.getContent();
                InternalMessage internalMessage = readAndProcessMessage(receiveContent);

                log.info("Agent {} receive message: {}", getName(), receiveContent);
                String content = writeMessageAsString(internalMessage, receiveContent);

                cfp.setConversationId(TaskStatus.TO_DO.toString());
                cfp.addReceiver(projectManager);
                cfp.setContent(content);
                log.info("Try to send received message from agent {} to {}", getName(), projectManager.getName());

                myAgent.send(cfp);
                log.info("Agent {} successfully send message: [messageType: {}, messageContent: {}] to [name: {}]", getName(), ACLMessage.getPerformative(cfp.getPerformative()), content, projectManager.getName());
            } else {
                log.info("Agent {} could not receive message. Wait new messages", getName());
                block();
            }

        }
    }

}
