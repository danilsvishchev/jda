package agent;

import enums.TaskStatus;
import jade.core.AID;
import jade.core.NotFoundException;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import model.InternalMessage;
import org.apache.commons.collections4.CollectionUtils;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Агента A2 (FORK) <br/>
 * Ожидает сообщений-заданий на обработку от {@link DeliveryManagerAgent} <br/>
 * Отдает на выполнение агентам-разработчикам {@link DeveloperAgent}
 */
@Getter
@Slf4j
public class ProjectManagerAgent extends AbstractAgent {
    private Set<AID> developers = new HashSet<>();

    public static final String AGENT_PUBLISHED_NAME = "Project manager";

    @Override
    protected String getPublishingType() {
        return "task-processing";
    }

    @Override
    public String getPublishingName() {
        return AGENT_PUBLISHED_NAME;
    }

    @Override
    protected void setup() {
        super.setup();

        findDevelopers();

        addBehaviour(new MessageService());
    }

    /**
     * Поиск агентов разработчиков {@link DeveloperAgent} для выполнения поставленных сообщений-тикетов
     */
    private void findDevelopers() {
        try {
            developers = Optional.ofNullable(findPublishedAgents("functional development", null))
                                 .orElseThrow(() -> new NotFoundException("Developers was not found in yellow pages service"));
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Обработчик входящих сообщений <br/>
     *
     * Осуществляется получение сообщение по заданному шаблону и отправка агенту {@link DeveloperAgent}
     */
    class MessageService extends CyclicBehaviour {

        @Override
        public void action() {
            if (CollectionUtils.isEmpty(developers)) {
                log.warn("Developers not found. Any messages could not be processed");
                findDevelopers();
                return;
            }

            log.info("Try to receive message by {}", getName());
            MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                           MessageTemplate.MatchConversationId(TaskStatus.TO_DO.toString()));
            ACLMessage message = myAgent.receive(template);

            if (message != null) {
                ACLMessage cfp = new ACLMessage(ACLMessage.INFORM);
                String receiveContent = message.getContent();
                InternalMessage internalMessage = readAndProcessMessage(receiveContent);

                log.info("Agent {} receive message: {}", getName(), receiveContent);
                String content = writeMessageAsString(internalMessage, receiveContent);

                cfp.setConversationId(TaskStatus.IN_PROGRESS.toString());
                developers.forEach(cfp::addReceiver);
                cfp.setContent(content);
                log.info("Try to send received message from agent {} to {}", getName(), developers.stream().map(AID::getName).collect(Collectors.toList()));

                myAgent.send(cfp);
                log.info("Agent {} successfully send message: [messageType: {}, messageContent: {}] to developers {}", getName(), ACLMessage.getPerformative(cfp.getPerformative()), content, developers.stream().map(AID::getName).collect(Collectors.toList()));
            } else {
                log.info("Agent {} could not receive message. Wait new messages", getName());
                block();
            }
        }
    }

}
