package agent;

import com.fasterxml.jackson.core.JsonProcessingException;
import enums.TaskStatus;
import jade.core.AID;
import jade.core.NotFoundException;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import model.InternalMessage;
import model.ProcessableObject;
import org.apache.commons.collections4.CollectionUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Агент A6 (JOIN) <br/>
 * Ожидает сообщений-заданий на обработку от {@link DeveloperAgent} <br/>
 * Отдает на выполнение агенту-руководителю тестирования {@link TestDirectorAgent}
 */
@Getter
@Slf4j
public class TeamLeadAgent extends AbstractAgent {
    private final Map<String, ProcessableObject> processableObjects = new HashMap<>();
    private Set<AID> developers = new HashSet<>();
    private AID testingLead;

    @Override
    protected String getPublishingType() {
        return "task-accepting-and-merging";
    }

    @Override
    protected String getPublishingName() {
        return "Team lead";
    }

    @Override
    protected void setup() {
        super.setup();
        findDevelopers();
        findTestingDirector();

        addBehaviour(new TickerBehaviour(this, 60_000L) {
            @Override
            public void onTick() {
                findDevelopers();
                findTestingDirector();
            }
        });

        addBehaviour(new MessageService());
    }

    /**
     * Поиск агента руководителя тестирования {@link TestDirectorAgent} в сервисе желтых страниц
     */
    private void findTestingDirector() {
        try {
            testingLead = findPublishedAgents("task-test-processing", "Test Director").stream()
                                                                                                            .findFirst()
                                                                                                            .orElseThrow(() -> new NotFoundException("Testing lead was not found in yellow pages service"));
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Поиск агентов разработчиков {@link DeveloperAgent} для выполнения поставленных сообщений-тикетов
     */
    private void findDevelopers() {
        try {
            developers = Optional.ofNullable(findPublishedAgents("functional development", null))
                                 .orElseThrow(() -> new NotFoundException("Developers was not found in yellow pages service"));
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Поведение, описывающее поиск и обработку входящих сообщений
     */
    class MessageService extends CyclicBehaviour {

        @Override
        public void action() {
            if (testingLead == null || CollectionUtils.isEmpty(developers)) {
                log.warn("Receiver AID or senders not found. Any messages could not be processed");
                findDevelopers();
                findTestingDirector();
                return;
            }

            log.info("Try to receive message by {}", getName());
            MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                           MessageTemplate.MatchConversationId(TaskStatus.IN_REVIEW.toString()));
            ACLMessage message = myAgent.receive(template);

            if (message != null) {
                String receiveContent = message.getContent();
                InternalMessage internalMessage = readAndProcessMessage(receiveContent);

                log.info("Agent {} receive message: {}", getName(), receiveContent);

                if (!processableObjects.containsKey(internalMessage.getId())) {
                    processableObjects.put(internalMessage.getId(), new ProcessableObject(internalMessage, developers.size() - 1));
                    return;
                }

                ProcessableObject processableObject = processableObjects.get(internalMessage.getId());
                int lastHandler = internalMessage.getHandlers().size() - 1;

                processableObject.getMessage().setUpdateTimestamp(internalMessage.getUpdateTimestamp());
                processableObject.getMessage().getHandlers().add(internalMessage.getHandlers().get(lastHandler));
                processableObject.decrement();

                if (processableObject.getProcessingCycle() == 0) {
                    processableObjects.remove(internalMessage.getId());
                    processableObject.getMessage().getHandlers().add(getAID());
                    sendMessage(writeMessageAsString(processableObject.getMessage(), receiveContent));
                }
            } else {
                log.info("Agent {} could not receive message. Wait new messages", getName());
                block();
            }
        }

        /**
         * Отправка входящих сообщений
         */
        private void sendMessage(String message) {
            log.info("Try to send message from agent {} to {}", getName(), testingLead.getName());

            ACLMessage cfp = new ACLMessage(ACLMessage.INFORM);

            cfp.setConversationId(TaskStatus.TO_CASE_ANALYSE.toString());
            cfp.addReceiver(testingLead);
            cfp.setContent(message);

            myAgent.send(cfp);
            log.info("Agent {} successfully send message: [messageType: {}, messageContent: {}] to [name: {}]", getName(), ACLMessage.getPerformative(cfp.getPerformative()), message, testingLead.getName());
        }
    }

    /**
     * Переопределение метода абстрактного класса {@link AbstractAgent#readAndProcessMessage}. <br/>
     * Десериазация полученного сообщения
     *
     * @param receiveMessage полученное сообщение
     * @return объект внутреннего сообщения
     */
    @Override
    protected InternalMessage readAndProcessMessage(String receiveMessage) {
        try {
            return mapper.readValue(receiveMessage, InternalMessage.class);
        } catch (JsonProcessingException e) {
            log.error("Couldn't deserialize message content: {}", receiveMessage);
            throw new IllegalArgumentException(e.getMessage());
        }
    }
}
