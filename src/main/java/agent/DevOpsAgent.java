package agent;

import enums.TaskStatus;
import jade.core.AID;
import jade.core.NotFoundException;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import model.InternalMessage;
import org.apache.commons.collections4.CollectionUtils;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Агента A13 (OR) <br/>
 * Ожидает сообщения-задания на обработку от {@link TesterAgent} <br/>
 * Отдает конечный результат агенту-клиенту {@link  ClientAgent}
 */
@Getter
@Slf4j
public class DevOpsAgent extends AbstractAgent {
    private Set<AID> testers = new HashSet<>();
    private AID client;

    public static final String AGENT_PUBLISHED_NAME = "DevOps";

    @Override
    protected String getPublishingType() {
        return "task-support-and-deployment";
    }

    @Override
    protected String getPublishingName() {
        return AGENT_PUBLISHED_NAME;
    }

    @Override
    protected void setup() {
        super.setup();

        findTesters();
        findClient();

        addBehaviour(new TickerBehaviour(this, 60_000L) {
            @Override
            public void onTick() {
                findTesters();
                findClient();
            }
        });

        addBehaviour(new MessageService());
    }


    /**
     * Поиск агента-клиента (пользователя) {@link ClientAgent} в сервисе желтых страниц
     */
    private void findClient() {
        try {
            client = findPublishedAgents("use-of-functionality", "Client").stream()
                    .findFirst()
                    .orElseThrow(() -> new NotFoundException("Client was not found in yellow pages service"));
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Поиск агентов тестировщиков {@link TesterAgent}
     */
    private void findTesters() {
        try {
            testers = Optional.ofNullable(findPublishedAgents("functional testing", null))
                    .orElseThrow(() -> new NotFoundException("Testers was not found in yellow pages service"));
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Поведение, описывающее поиск и обработку входящих сообщений
     */
    class MessageService extends CyclicBehaviour {

        @Override
        public void action() {
            if (client == null || CollectionUtils.isEmpty(testers)) {
                log.warn("Receiver or senders AID not found. Any messages could not be processed");
                findTesters();
                findClient();
                return;
            }

            log.info("Try to receive message by {}", getName());
            MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                           MessageTemplate.MatchConversationId(TaskStatus.TO_STAGE.toString()));
            ACLMessage message = myAgent.receive(template);

            if (message != null) {
                AID sender = Optional.ofNullable(message.getSender()).orElseGet(() -> new AID("default", false));

                if (testers.contains(sender)) {
                    String receiveContent = message.getContent();
                    InternalMessage internalMessage = readAndProcessMessage(receiveContent);

                    log.info("Agent {} receive message: {}", getName(), receiveContent);

                    processMessage(writeMessageAsString(internalMessage, receiveContent));
                } else {
                    log.warn("Receive message from unknown agent {}", sender);
                }

            } else {
                log.info("Agent {} could not receive message. Wait new messages", getName());
                block();
            }
        }

        /**
         * Отправка входящих сообщений
         */
        private void processMessage(String message) {
                log.info("Try to send message from agent {} to {}", getName(), client.getName());

                ACLMessage cfp = new ACLMessage(ACLMessage.INFORM);

                cfp.setConversationId(TaskStatus.DONE.toString());
                cfp.addReceiver(client);
                cfp.setContent(message);

                myAgent.send(cfp);
                log.info("Agent {} successfully send message: [messageType: {}, messageContent: {}] to [name: {}]", getName(), ACLMessage.getPerformative(cfp.getPerformative()), message, client.getName());
        }
    }

}
