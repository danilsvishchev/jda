package agent;

import enums.TaskStatus;
import jade.core.AID;
import jade.core.NotFoundException;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import model.InternalMessage;

/**
 * Агента A10-A12 <br/>
 * Ожидает сообщений-заданий на обработку от {@link TestLeadAgent} <br/>
 * Отдает на выполнение агенту-инженеру поддержки {@link DevOpsAgent}
 */
@Getter
@Slf4j
public class TesterAgent extends AbstractAgent {
    private AID devOps;

    @Override
    protected String getPublishingType() {
        return "functional testing";
    }

    @Override
    protected String getPublishingName() {
        return "Tester " + System.currentTimeMillis();
    }

    @Override
    protected void setup() {
        super.setup();

        findDevOps();

        addBehaviour(new MessageService());
    }

    /**
     * Поиск агента инженера поддержки {@link DevOpsAgent} для поддержки и развертывания выполненных задач
     */
    private void findDevOps() {
        try {
            devOps = findPublishedAgents("task-support-and-deployment", "DevOps").stream()
                                                                                                    .findFirst()
                                                                                                    .orElseThrow(() -> new NotFoundException("Devops was not found in yellow pages service"));
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Обработчик входящих сообщений <br/>
     *
     * Осуществляется получение сообщение по заданному шаблону и отправка агенту {@link DevOpsAgent}
     */
    class MessageService extends CyclicBehaviour {

        @Override
        public void action() {
            if (devOps == null) {
                log.warn("Receiver AID not found. Any messages could not be processed");
                findDevOps();
                return;
            }

            log.info("Try to receive message by {}", getName());
            MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                           MessageTemplate.MatchConversationId(TaskStatus.IN_TESTING.toString()));
            ACLMessage message = myAgent.receive(template);

            if (message != null) {
                ACLMessage cfp = new ACLMessage(ACLMessage.INFORM);
                String receiveContent = message.getContent();
                InternalMessage internalMessage = readAndProcessMessage(receiveContent);

                log.info("Agent {} receive message: {}", getName(), receiveContent);
                String content = writeMessageAsString(internalMessage, receiveContent);

                cfp.setConversationId(TaskStatus.TO_STAGE.toString());
                cfp.addReceiver(devOps);
                cfp.setContent(content);
                log.info("Try to send received message from agent {} to {}", getName(), devOps.getName());

                myAgent.send(cfp);
                log.info("Agent {} successfully send message: [messageType: {}, messageContent: {}] to [name: {}]", getName(), ACLMessage.getPerformative(cfp.getPerformative()), content, devOps.getName());
            } else {
                log.info("Agent {} could not receive message. Wait new messages", getName());
                block();
            }
        }
    }

}
