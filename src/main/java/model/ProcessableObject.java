package model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProcessableObject {
    private InternalMessage message;
    private int processingCycle;

    public void decrement() {
        processingCycle--;
    }
}
