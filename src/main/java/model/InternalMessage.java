package model;

import jade.core.AID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InternalMessage {
    /**
     * Идентификатор сообщения
     */
    private String id;
    /**
     * Дата и время создания сообщения в миллисекундах
     */
    private long createTimestamp;
    /**
     * Идентификатор создателя сообщения
     */
    private AID authorAID;
    /**
     * Дата и время обновления сообщения в миллисекундах
     */
    private long updateTimestamp;
    /**
     * Идентификаторы агентов, обработавших сообщение
     */
    private List<AID> handlers = new ArrayList<>();
    /**
     * Содержание сообщения
     */
    private String content;
}
