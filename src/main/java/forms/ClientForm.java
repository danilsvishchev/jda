package forms;

import agent.ClientAgent;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;

/**
 * Интерфейс агента A14
 *
 * @see agent.ClientAgent
 */
@Getter
@Slf4j
public class ClientForm extends JFrame {
    private static final String ERROR_HEADER = "Error";

    private final ClientAgent agent;
    private final JButton addMessageButton;
    private final JButton clearSelectedButton;
    private final JButton editMessageButton;
    private final JLabel messageLabel;
    private final JScrollPane mainPane;
    private final JList<String> messageList;
    private final JTextField messageField;
    private final JButton sendMessageButton;

    public ClientForm(ClientAgent agent) {
        this.agent = agent;
        this.mainPane = new JScrollPane();
        this.messageList = new JList<>();
        this.messageField = new JTextField();
        this.addMessageButton = new JButton();
        this.sendMessageButton = new JButton();
        this.clearSelectedButton = new JButton();
        this.editMessageButton = new JButton();
        this.messageLabel = new JLabel();

        initComponents();
        initScreen();
    }

    private void initComponents() {
        log.info("Init form components");

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("A14");

        messageList.setModel(new DefaultListModel<>());
        messageList.addListSelectionListener(this::messageListValueChanged);
        messageList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        mainPane.setViewportView(messageList);

        messageField.setText("Enter message");

        addMessageButton.setText("Add");
        addMessageButton.addActionListener(this::addMessageButtonActionPerformed);

        sendMessageButton.setText("Send");
        sendMessageButton.addActionListener(this::sendMessageButtonActionPerformed);

        clearSelectedButton.setText("Clear");
        clearSelectedButton.addActionListener(this::clearSelectedButtonActionPerformed);

        editMessageButton.setText("Edit");
        editMessageButton.addActionListener(this::editMessageButtonActionPerformed);

        messageLabel.setText("Active messages");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(sendMessageButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(addMessageButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(messageField, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE)
                    .addComponent(clearSelectedButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(editMessageButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(mainPane, GroupLayout.PREFERRED_SIZE, 288, GroupLayout.PREFERRED_SIZE)
                    .addComponent(messageLabel))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(messageField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(addMessageButton)
                        .addGap(18, 18, 18)
                        .addComponent(sendMessageButton)
                        .addGap(18, 18, 18)
                        .addComponent(clearSelectedButton)
                        .addGap(18, 18, 18)
                        .addComponent(editMessageButton))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(messageLabel)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mainPane, GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap(52, Short.MAX_VALUE))
        );

        pack();
    }

    private void initScreen() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int centerX = (int)screenSize.getWidth() / 2;
        int centerY = (int)screenSize.getHeight() / 2;
        setLocation(centerX - getWidth() / 2, centerY - getHeight() / 2);
        this.setResizable(false);
    }

    private void addMessageButtonActionPerformed(ActionEvent evt) {
        String message = messageField.getText();
        if (StringUtils.isBlank(message)) {
            log.error("Message field text is not set");
            JOptionPane.showMessageDialog(this, "Please fill message field", ERROR_HEADER, JOptionPane.ERROR_MESSAGE);
            return;
        }

        log.info("Add new message to queue: \"{}\"", message);
        DefaultListModel<String> model = (DefaultListModel<String>) messageList.getModel();
        model.addElement(message);
        messageField.setText("");
    }

    private void sendMessageButtonActionPerformed(ActionEvent evt) {
        String message = messageList.getSelectedValue();
        if (StringUtils.isBlank(message)) {
            log.error("Message not selected to send");
            JOptionPane.showMessageDialog(this, "Please select message", ERROR_HEADER, JOptionPane.ERROR_MESSAGE);
            return;
        }

        log.info("Send selected message: \"{}\" to agent", message);
        agent.sendMessage(message);

        log.info("Remove selected values from list");
        DefaultListModel<String> model = (DefaultListModel<String>) messageList.getModel();
        model.removeElement(message);
    }

    private void clearSelectedButtonActionPerformed(ActionEvent evt) {
        String message = messageList.getSelectedValue();
        if (StringUtils.isBlank(message)) {
            log.error("Message not selected to delete");
            JOptionPane.showMessageDialog(this, "Please select message", ERROR_HEADER, JOptionPane.ERROR_MESSAGE);
            return;
        }

        log.info("Remove selected value from list");
        DefaultListModel<String> model = (DefaultListModel<String>) messageList.getModel();
        model.removeElement(message);
    }

    private void editMessageButtonActionPerformed(ActionEvent evt) {
        String selectedMessage = messageList.getSelectedValue();
        String message = messageField.getText();
        if (StringUtils.isBlank(message)) {
            log.error("Message field text is not set to edit");
            JOptionPane.showMessageDialog(this, "Please select message to edit", ERROR_HEADER, JOptionPane.ERROR_MESSAGE);
            return;
        }

        log.info("Edit message \"{}\" to \"{}\"", selectedMessage, message);
        DefaultListModel<String> model = (DefaultListModel<String>) messageList.getModel();
        model.removeElement(selectedMessage);
        model.addElement(message);
    }

    private void messageListValueChanged(ListSelectionEvent evt) {
        messageField.setText(messageList.getSelectedValue());
    }

}
