package forms;

import agent.TestLeadAgent;
import jade.core.AID;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;

/**
 * Интерфейс агента А9
 *
 * @see agent.TestLeadAgent
 */
@Getter
@Slf4j
public class TestLeadForm extends JFrame {
    private static final String ERROR_HEADER = "Error";

    private final TestLeadAgent agent;
    private final JButton declineMessageButton;
    private final JLabel agentLabel;
    private final JList<AID> agentList;
    private final JScrollPane agentPane;
    private final JScrollPane messagePane;
    private final JLabel messageLabel;
    private final JList<String> messageList;
    private final JButton sendMessageButton;
    private final JButton updateAgentsButton;

    public TestLeadForm(TestLeadAgent agent) {
        this.agent = agent;
        this.agentPane = new JScrollPane();
        this.agentList = new JList<>();
        this.messagePane = new JScrollPane();
        this.messageList = new JList<>();
        this.agentLabel = new JLabel();
        this.messageLabel = new JLabel();
        this.sendMessageButton = new JButton();
        this.updateAgentsButton = new JButton();
        this.declineMessageButton = new JButton();

        initComponents();
        initScreen();
    }

    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("A9 (SWITCH)");

        agentList.setModel(new DefaultListModel<>());
        agentList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        agentPane.setViewportView(agentList);

        messageList.setModel(new DefaultListModel<>());
        messageList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        messagePane.setViewportView(messageList);

        agentLabel.setText("Active agents");

        messageLabel.setText("Active messages");

        sendMessageButton.setText("Send");
        sendMessageButton.addActionListener(this::sendMessageButtonActionPerformed);

        updateAgentsButton.setText("Update");
        updateAgentsButton.addActionListener(this::updateAgentsButtonActionPerformed);

        declineMessageButton.setText("Decline");
        declineMessageButton.addActionListener(this::declineMessageButtonActionPerformed);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(agentPane, GroupLayout.PREFERRED_SIZE, 259, GroupLayout.PREFERRED_SIZE)
                            .addComponent(agentLabel))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(messagePane, GroupLayout.PREFERRED_SIZE, 274, GroupLayout.PREFERRED_SIZE)
                            .addComponent(messageLabel)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(158, 158, 158)
                        .addComponent(updateAgentsButton, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(sendMessageButton, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(declineMessageButton, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(agentLabel)
                    .addComponent(messageLabel))
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(messagePane, GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(agentPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(sendMessageButton)
                    .addComponent(updateAgentsButton)
                    .addComponent(declineMessageButton))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }

    protected void initScreen() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int centerX = (int)screenSize.getWidth() / 2;
        int centerY = (int)screenSize.getHeight() / 2;
        setLocation(centerX - getWidth() / 2, centerY - getHeight() / 2);
        this.setResizable(false);
    }

    private void sendMessageButtonActionPerformed(ActionEvent evt) {
        String message = messageList.getSelectedValue();
        AID agentAID = agentList.getSelectedValue();

        if (StringUtils.isBlank(message)) {
            log.error("Message not selected to send");
            JOptionPane.showMessageDialog(this, "Please select message", ERROR_HEADER, JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (agentAID == null) {
            log.error("Agent (receiver) not selected for send message");
            JOptionPane.showMessageDialog(this, "Please select agent", ERROR_HEADER, JOptionPane.ERROR_MESSAGE);
            return;
        }

        log.info("Select message \"{}\" and agent {}", message, agentAID);
        agent.sendMessage(message, agentAID);

        log.info("Remove message \"{}\" from active list", message);
        DefaultListModel<String> model = (DefaultListModel<String>) messageList.getModel();
        model.removeElement(message);
    }

    private void updateAgentsButtonActionPerformed(ActionEvent evt) {
        log.info("Force update testers");
        agent.findTesters();
    }

    private void declineMessageButtonActionPerformed(ActionEvent evt) {
        String message = messageList.getSelectedValue();
        if (StringUtils.isBlank(message)) {
            log.error("Message not selected to send");
            JOptionPane.showMessageDialog(this, "Please select message", ERROR_HEADER, JOptionPane.ERROR_MESSAGE);
            return;
        }

        log.info("Decline message \"{}\"", message);
        DefaultListModel<String> model = (DefaultListModel<String>) messageList.getModel();
        model.removeElement(message);
    }

}
