package forms;

import agent.TechnicalDirectorAgent;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.List;

/**
 * Интерфейс агента A0
 *
 * @see agent.TechnicalDirectorAgent
 */
@Getter
@Slf4j
public class TechDirectorForm extends JFrame {
    private static final String ERROR_HEADER = "Error";

    private final TechnicalDirectorAgent agent;
    private final JButton addMessageButton;
    private final JButton clearAllButton;
    private final JButton clearSelectedButton;
    private final JButton editMessageButton;
    private final JList<String> messageList;
    private final transient ListModel<String> listModel;
    private final JButton sendMessageButton;
    private final JScrollPane jScrollPane;
    private final JTextField messageField;

    public TechDirectorForm(TechnicalDirectorAgent agent) {
        this.agent = agent;
        this.messageField = new JTextField();
        this.jScrollPane = new JScrollPane();
        this.messageList = new JList<>();
        this.listModel = new DefaultListModel<>();
        this.addMessageButton = new JButton();
        this.sendMessageButton = new JButton();
        this.clearSelectedButton = new JButton();
        this.clearAllButton = new JButton();
        this.editMessageButton = new JButton();

        initComponents();
        initScreen();
    }

    private void initComponents() {
        log.info("Init form components");

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("A0");

        messageField.setText("Enter message");

        messageList.setModel(listModel);
        messageList.addListSelectionListener(this::messageListValueChanged);
        messageList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        jScrollPane.setViewportView(messageList);

        addMessageButton.setText("Add");
        addMessageButton.addActionListener(this::addMessageButtonActionPerformed);

        sendMessageButton.setText("Send");
        sendMessageButton.addActionListener(this::sendMessageButtonActionPerformed);

        clearSelectedButton.setText("Clear");
        clearSelectedButton.addActionListener(this::clearSelectedButtonActionPerformed);

        clearAllButton.setText("Clear all");
        clearAllButton.addActionListener(this::clearAllButtonActionPerformed);

        editMessageButton.setText("Edit");
        editMessageButton.addActionListener(this::editMessageButtonActionPerformed);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(messageField, GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                    .addComponent(addMessageButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sendMessageButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(clearSelectedButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(clearAllButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(editMessageButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                .addComponent(jScrollPane, GroupLayout.PREFERRED_SIZE, 352, GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane, GroupLayout.PREFERRED_SIZE, 238,GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(messageField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(addMessageButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sendMessageButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(clearSelectedButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(clearAllButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(editMessageButton)))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        pack();
    }

    private void initScreen() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int centerX = (int)screenSize.getWidth() / 2;
        int centerY = (int)screenSize.getHeight() / 2;
        setLocation(centerX - getWidth() / 2, centerY - getHeight() / 2);
        this.setResizable(false);
    }

    private void addMessageButtonActionPerformed(ActionEvent evt) {
        String message = messageField.getText();
        if (StringUtils.isBlank(message)) {
            log.error("Message field text is not set");
            JOptionPane.showMessageDialog(this, "Please fill message field", ERROR_HEADER, JOptionPane.ERROR_MESSAGE);
            return;
        }

        log.info("Add new message to queue: \"{}\"", message);
        DefaultListModel<String> model = (DefaultListModel<String>) messageList.getModel();
        model.addElement(message);
        messageField.setText("");
    }

    private void sendMessageButtonActionPerformed(ActionEvent evt) {
        List<String> values = messageList.getSelectedValuesList();
        if (CollectionUtils.isEmpty(values)) {
            log.error("Message not selected to send");
            JOptionPane.showMessageDialog(this, "Please select message", ERROR_HEADER, JOptionPane.ERROR_MESSAGE);
            return;
        }

        String message = StringUtils.join(values, ",");
        log.info("Send selected message: \"{}\" to agent", message);
        agent.sendMessage(message);

        log.info("Remove selected values from list");
        DefaultListModel<String> model = (DefaultListModel<String>) messageList.getModel();
        values.forEach(model::removeElement);
    }

    private void clearSelectedButtonActionPerformed(ActionEvent evt) {
        List<String> values = messageList.getSelectedValuesList();
        if (CollectionUtils.isEmpty(values)) {
            log.error("Message not selected to delete");
            JOptionPane.showMessageDialog(this, "Please select message", ERROR_HEADER, JOptionPane.ERROR_MESSAGE);
            return;
        }

        log.info("Remove selected values from list");
        DefaultListModel<String> model = (DefaultListModel<String>) messageList.getModel();
        values.forEach(model::removeElement);
    }

    private void clearAllButtonActionPerformed(ActionEvent evt) {
        log.info("Remove all values from list");
        DefaultListModel<String> model = (DefaultListModel<String>) messageList.getModel();
        model.removeAllElements();
    }

    private void editMessageButtonActionPerformed(ActionEvent evt) {
        String selectedMessage = messageList.getSelectedValue();
        String message = messageField.getText();
        if (StringUtils.isBlank(message)) {
            log.error("Message field text is not set to edit");
            JOptionPane.showMessageDialog(this, "Please select message to edit", ERROR_HEADER, JOptionPane.ERROR_MESSAGE);
            return;
        }

        log.info("Edit message \"{}\" to \"{}\"", selectedMessage, message);
        DefaultListModel<String> model = (DefaultListModel<String>) messageList.getModel();
        model.removeElement(selectedMessage);
        model.addElement(message);
    }

    private void messageListValueChanged(ListSelectionEvent evt) {
        messageField.setText(messageList.getSelectedValue());
    }
}
